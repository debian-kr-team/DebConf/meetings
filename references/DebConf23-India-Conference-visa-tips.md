﻿Some things we learned while organizing DebConf 23 at Kochi, sharing this in case some one organizing an international conference in future may find this useful.  

1. Conference visa is strongly recommended, if someone is already in India for tourism they have to intimate FRRO (Foreigners Regional Registration Office) before the conference.  

    1.1 e-visa can happen in1-2 days but people applying at missions may need upto a week or longer to get the visa.  

    1.2 People coming on conference visa can combine tourism (but not the reverse).  

    1.3 For conference visa, Invitation from organizer (mandatory), Political Clearance from Ministry of External Affairs (MEA) (mandatory) and Ministry of Home Affairs (MHA) clearance (If required, see #3).  

2. Close registrations for foreigners at least a month before the conference (the earlier the better) as you will need to share the full participants list including passport details with MEA for political clearance (this can take upto 2-3 weeks).  

    2.1 You need to contact Coordination department of MEA. Send email to SO (COORD) from [https://videshapps.gov.in/videsh/divsion\_data/342](https://videshapps.gov.in/videsh/divsion_data/342) You may call them for updates.  

    2.2 For participants from some countries, they may ask for their CV and profile (speaker or participant).  

    2.3 The request need to be in a letter head of a registered organization (signed and scanned).  

    2.4 It may be easier to collect the passport details along with registration form.  

3. If you have participants from countries that need security clearance from MHA (countries like Pakistan, Sudan and stateless persons/undefined nationality), you might have to close registrations much earlier as MHA clearance should be applied 60 days before the conference for private organizers and 30 days before for government institutions.  

    3.1 MEA clearance is a pre condition to MHA clearance.  

    3.2 If you have such participants or participants from countries with a bad bilateral relations, these clearances can take even longer or may never happen.  

    3.3 You can apply for MHA clearance from [https://conference.mha.gov.in/](https://conference.mha.gov.in/)  

4. They will need an invitation letter from organizers.  

    4.1 The invitation letter should contain who will be paying for their travel, food and accommodation.  

    4.2 Also personal details like passport number, country, home address, nationality.  

    4.3 They will also need a reference address when applying for e-Visa.  

5. You will need to intimate the FRRO Office before the conference.  

    5.1 You may get calls from local police asking for details about the conference. You may need to share the accommodation details and list of countries.  

6. For attendees: see [https://debconf23.debconf.org/about/visas/](https://debconf23.debconf.org/about/visas/)
