Meeting in DebConf23
===

1. 소통 채널
  - 소통 주기 : 1개월 마다
  - 소통 채널
    - Discord
      - 현재 초대만 가능
      - 링크 불가 - Nitro Boost 월간 결제 필요함
    - IRC
      - Matrix로 생성 시도하였으나 실패
      - 추후 다시 생각 ~~

2. DebConf in Korea
  - 비딩 페이지 : https://wiki.debian.org/DebConf/26/Bids/Korea
  - 짧은 버전 PPT : [Google Slides](https://docs.google.com/presentation/d/1uI7fUBxrcdFdX9v7PIaR84BBA9HIEokUd7j2tdD7WXY)
    - DebConf23에서 발표하려 함
    - 흥미 유도용, 부산 위주로 소개
    - 할 수 있을까 ..
  - 긴 버전 PPT : [Google Slides](https://docs.google.com/presentation/d/1r-EEiOUtZnNAb-Ug1s6iG17bu2pa3pFIiOEf3jK9QDA)
    - DebConf24에서 발표?
  - Venue candidates
    - 학교
      - (부산) 부경대 : [송하주 교수님](https://db.pknu.ac.kr/)
        - 학회 연계?
          - [정보과학회](http://www.kiise.or.kr/)
      - (부산) 한국해양대 : [허준호 교수님](https://www.kmou.ac.kr/di/ad/tepDept/main/view.do?mi=1657&teaSn=2760)
      - (서울) 서울대 ?

